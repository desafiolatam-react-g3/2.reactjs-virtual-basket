EJERCICIO 2: CANASTA VIRTUAL
---
Se debe desarrollar una Canasta Virtual que guarde los artículos que quieren ser comprados.

La aplicación debe agrupar artículos por su tipo, mediante estaciones (vistas). De esta forma 
las estaciones son las siguientes: Electrónicos, Películas y Video juegos.

El flujo de compra de artículos debe recorrer todas las estaciones de forma secuencial.

La canasta debe iniciar vacía, y luego, al ir avanzando por cada estación, se podrán agregar artículos por estación.

Al finalizar, luego de pasar por las 3 estaciones, se debe mostrar una vista de detalles de compra con el contenido de la canasta y el valor total de la compra.

Además, en la vista de detalles de compra se debe permitir volver a elegir alguna estación y escoger otro artículo. En este flujo específico, al terminar de seleccionar artículos, se debe retornar a la vista de detalles de compra.

Para finalizar el flujo, en la vista de detalles de compra, debe existir un botón "Comprar" que al ser presionado muestre un mensaje de compra exitosa y limpie la canasta para iniciar un nuevo flujo de compra.

### Fuentes de datos
- Electrónicos: `./public/electorics.json`
- Películas: `./public/movies.json`
- Video juegos: `./public/videogames.json`

## Recursos
- [Documentación oficial de Reactjs](https://reactjs.org/docs)
- [Documentación oficial de Reduxjs](https://redux.js.org/)
- [Repositorio oficial de Create React App](https://github.com/facebook/create-react-app)

***
© [DesafioLatam](https://desafiolatam.com) - Todos los derechos reservados
